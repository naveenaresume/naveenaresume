import 'package:flutter/material.dart';
void main() {
  runApp(const MyApp());
}
class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My Resume',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.blue),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'My Resume Home Page'),
    );
  }
}
class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}
class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body:
      ListView(
           children: [
             Stack(
              children:
             <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Container(

                    height: 130,
                    width:660,
                    color: Color(0xFF043381),
                    alignment: Alignment.bottomLeft,
                  ),
                ),
               Padding(
                 padding: const EdgeInsets.only(left: 400,bottom: 0,top: 20),
                 child: Container(
                   height: 110,
                   width:250,
                   alignment: Alignment.topLeft,
                   // color: Colors.black,

                   child: Padding(
                     padding: const EdgeInsets.only(left:0),
                     child: Stack(

                       children: [
                         Text('Contact Information',
                           style: TextStyle(
                             fontSize: 14,
                             color: Colors.white,
                             fontWeight: FontWeight.bold,
                             fontFamily: 'sans-serif',
                           ),
                         ),
                         SizedBox(height: 5,),
                         Padding(
                           padding: const EdgeInsets.only(right: 0,top: 22,left: 10),
                           child: Icon(
                             Icons.email,
                               color:Colors.white,
                             size: 18,
                           ),
                         ),
                         Padding(
                           padding: const EdgeInsets.only(right: 0,left: 36,top: 20,),
                           child: Text('naveenas9092@gmail.com',
                           style: TextStyle(
                             fontSize: 14,
                             color: Colors.white,
                           ),),
                         ),
                         Padding(
                           padding: const EdgeInsets.only(top: 42,left: 10),
                           child: Icon(
                             Icons.call,
                             color: Colors.white,
                             size: 18,
                           ),
                         ),
                         Padding(
                           padding: const EdgeInsets.only(right: 0,left: 36,top: 42),
                           child: Text('+91 9361798667',
                             style: TextStyle(
                               fontSize: 14,
                               color: Colors.white,
                             ),),
                         ),
                         Padding(
                           padding: const EdgeInsets.only(top: 67,left: 10),
                           child: Icon(
                             Icons.account_balance_wallet_rounded,
                             color: Colors.white,
                             size: 16,
                           ),
                         ),
                         Padding(
                           padding: const EdgeInsets.only(right: 0,left: 36,top: 64),
                           child: Text('https\'//www.linkedin.com/in\n/navee-na-s-064406266',
                             style: TextStyle(
                               fontSize: 14,
                               color: Colors.white,
                             ),),
                         ),
                       ],
                     ),
                   ),
                 ),
               ),
               Padding(
                 padding: const EdgeInsets.only(top: 6,left: 10),
                 child: Container(
                   height: 124,
                   width:350,
                   child: Transform(
                     transform: Matrix4.skewX(-0.2),
                     child: Container(
                       height: 110,
                       width: 80,
                       color: Color(0xFF042865),
                       child: Padding(
                         padding: const EdgeInsets.only(left: 110,top: 78),
                         child: Stack(
                           children: [
                             Padding(
                               padding: const EdgeInsets.only(bottom: 19,left: 5),
                               child: Text('NAVEENA S',
                                 style: TextStyle(
                                   fontSize: 20,
                                   color: Color(0xFFFFFFFF),
                                   fontWeight: FontWeight.bold,
                                   fontFamily: 'sans-serif',
                                 ),),
                             ),
                             Padding(
                               padding: const EdgeInsets.only(top:24,left: 8),
                               child: Text('ELECTRICAL AND ELECTRONICS ENGINEERING',
                                 style: TextStyle(
                                   fontSize: 10,
                                   color: Color(0xFFFFFFFF),
                                   fontWeight: FontWeight.bold,
                                   fontFamily: 'sans-serif',
                                 ),),
                             ),
                           ],
                         ),
                       ),
                     ),
                   ),
                 ),
               ),
                Padding(
                  padding: const EdgeInsets.only(top: 20,left: 20),
                  child: Container(
                    height: 100,
                    width:80,
                    child: Image.asset('assets/photo.jpg',
                      fit: BoxFit.cover,),
                    color: Color(0xFFC7C7E0),
                  ),
                ),
               Padding(
                 padding: const EdgeInsets.only(top: 140,left: 18),
                 child: Container(
                   color: Color(0xFF181819),
                   height: 950,
                   width:250,
                   alignment: Alignment.topCenter,
                     // padding: const EdgeInsets.only(top: 145),
                     child: Padding(
                       padding: const EdgeInsets.only(top: 8.0),
                       child:  Stack(
                         children: [
                         Container(
                           alignment: Alignment.topCenter,
                           child: Text(
                           'About Me',
                           style: TextStyle(
                             color: Colors.white,
                           ),),
                         ),
                           Padding(
                             padding: const EdgeInsets.only(top: 25,left: 5),
                             child: Container(
                               // alignment: Alignment.topCenter,
                               height: 150,
                               width: 240,
                               // color: Colors.blue,
                               child: Text(
                                 'As an Enthusiastic Electrical and Electronics Engineering enthusiast with a strong academic background and hands on experience. Committed to precision, attention to detail, and continuous learnining in the dynamic EEE field.Eager to contribute skills to innovative projects that push technological boundaries and enhance the world.',
                                 style: TextStyle(
                                   fontSize: 12,
                                   color: Colors.white,
                                   height: 1.4,
                                 ),
                                 textAlign: TextAlign.justify,
                               ),
                             ),
                           ),
                           Padding(
                             padding: const EdgeInsets.only(top: 190,left: 45),
                             child: Container(
                               height: 28,
                               width: 160,
                               alignment: Alignment.center,
                               decoration: BoxDecoration(
                                 borderRadius: BorderRadius.circular(15),
                                 border: Border.all(color: Color(0xFF0772BA),width: 2,
                                 ),
                               ),
                                 child: Text('Skills',
                                 style: TextStyle(
                                   fontSize: 16,
                                   fontWeight: FontWeight.bold,
                                   color: Colors.white,
                                 ),)),
                           ),
                           Padding(
                             padding: const EdgeInsets.only(top: 450,left: 45),
                             child: Container(
                                 height: 28,
                                 width: 160,
                                 alignment: Alignment.center,
                                 decoration: BoxDecoration(
                                   borderRadius: BorderRadius.circular(15),
                                   border: Border.all(color: Color(0xFF0772BA),width: 2,
                                   ),
                                 ),
                                 child: Text('Interests',
                                   style: TextStyle(
                                     fontSize: 16,
                                     fontWeight: FontWeight.bold,
                                     color: Colors.white,
                                   ),)),
                           ),
                           Padding(
                             padding: const EdgeInsets.only(top: 560,left: 45),
                             child: Container(
                                 height: 28,
                                 width: 160,
                                 alignment: Alignment.center,
                                 decoration: BoxDecoration(
                                   borderRadius: BorderRadius.circular(15),
                                   border: Border.all(color: Color(0xFF0772BA),width: 2,
                                   ),
                                 ),
                                 child: Text('Education',
                                   style: TextStyle(
                                     fontSize: 16,
                                     fontWeight: FontWeight.bold,
                                     color: Colors.white,
                                   ),)),
                           ),
                           Padding(
                             padding: const EdgeInsets.only(top: 730,left: 45),
                             child: Container(
                                 height: 28,
                                 width: 160,
                                 alignment: Alignment.center,
                                 decoration: BoxDecoration(
                                   borderRadius: BorderRadius.circular(15),
                                   border: Border.all(color: Color(0xFF0772BA),width: 2,
                                   ),
                                 ),
                                 child: Text('Languages Known',
                                   style: TextStyle(
                                     fontSize: 16,
                                     fontWeight: FontWeight.bold,
                                     color: Colors.white,
                                   ),)),
                           ),
                           Padding(
                             padding: const EdgeInsets.only(top: 820,left: 33),
                             child: Container(
                                 height: 28,
                                 width: 190,
                                 alignment: Alignment.center,
                                 decoration: BoxDecoration(
                                   borderRadius: BorderRadius.circular(15),
                                   border: Border.all(color: Color(0xFF0772BA),width: 2,
                                   ),
                                 ),
                                 child: Text('Github link for resume',
                                   style: TextStyle(
                                     fontSize: 16,
                                     fontWeight: FontWeight.bold,
                                     color: Colors.white,
                                   ),)),
                           ),
                           Padding(
                             padding: const EdgeInsets.only(top: 226,left: 20),
                             child: Column(
                               children: [
                                 Text('> MicroController Programming\n> Arduino & Wokwi, Tinkercad\n> Figma & UIzard\n> Progamming In C & C++\n> HTML & CSS\n> Flutter & Dart(Basic)- App Dev',
                                 style: TextStyle(
                                   color: Colors.white,
                                   fontSize: 14,
                                 ),),
                                Padding(
                                  padding: const EdgeInsets.only(right: 130),
                                  child: Text('> Soft Skillss',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold,
                                        height:2,
                                      ),
                                  ),
                                ),
                                 Text('> Team Work & Leadership\n> Time Management\n> Self Motivated & Fast Learner',
                                   style: TextStyle(
                                     color: Colors.white,
                                     fontSize: 14,
                                   ),),
                                 Padding(
                                   padding: const EdgeInsets.only(top: 52),
                                   child: Text('> Microcontroller & Embedded \n   Sysytem\n> Web Development',
                                     style: TextStyle(
                                       color: Colors.white,
                                       fontSize: 14,
                                     ),),
                                 ),
                                 Padding(
                                   padding: const EdgeInsets.only(top: 52),
                                   child: Text('1. SSLC -                   86%\n2. H.S.C -                   86%',
                                     style: TextStyle(
                                       color: Colors.white,
                                       fontSize: 14,
                                     ),),
                                 ),
                                 Padding(
                                   padding: const EdgeInsets.only(left: 6),
                                   child: Text('NSHS School,Salem,Tamilnadu.',
                                     style: TextStyle(
                                       color: Colors.white,
                                       fontSize: 11,
                                     ),),
                                 ),
                                 SizedBox(height: 6,),
                                 Padding(
                                   padding: const EdgeInsets.only(top: 0),
                                   child: Text('3. BE(EEE) -        84CGPA',
                                     style: TextStyle(
                                       color: Colors.white,
                                       // height: 2.5,
                                       fontSize: 14,
                                     ),),
                                 ),
                                 Padding(
                                   padding: const EdgeInsets.only(left: 26),
                                   child: Text('Government College Of Engineering,\nSalem,Tamilnadu.',
                                     style: TextStyle(
                                       color: Colors.white,
                                       fontSize: 11,
                                     ),),
                                 ),
                                 Padding(
                                   padding: const EdgeInsets.only(top: 52),
                                   child: Text('> Tamil & Telugu\n> English',
                                     style: TextStyle(
                                       color: Colors.white,
                                       // height: 2.5,
                                       fontSize: 14,
                                     ),),
                                 ),
                                 Padding(
                                   padding: const EdgeInsets.only(top: 52),
                                   child: Text('https\'//www.linkedin.com/in\n/navee-na-s-064406266\n ( Using Flutter )',
                                     style: TextStyle(
                                       color: Colors.white,
                                       // height: 2.5,
                                       fontSize: 14,
                                     ),),
                                 ),
                               ],
                             ),
                           ),
                         ],
                       ),
                     ),
                 ),
               ),
                Padding(
                  padding: const EdgeInsets.only(top: 140,left: 268),
                  child: Container(
                    alignment: Alignment.topLeft,
                    height: 950,
                    width: 392,
                    // color: Color(0xD09FFFEE),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 10,left: 5),
                          child: Container(
                              height: 28,
                              width: 200,
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                color: Color(0xFF043382),
                                borderRadius: BorderRadius.circular(15),
                                border: Border.all(color: Color(0xFF043382),width: 2,
                                ),
                              ),
                              child: Text('Professional Experience',
                                style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                ),)),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10,top: 10),
                          child: Text('IDEATION SPRINT PROGRAM - 7 Days',style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontSize: 18,
                          ),),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10,top: 0),
                          child: Text('Forge Innovation & Ventures, Coimbatore ',style: TextStyle(
                            fontWeight: FontWeight.normal,
                            color: Colors.black,
                            fontSize: 14,
                            // height: .8,
                          ),),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20,top: 0),
                          child: Text('> Collaborated with a team to ideate, design & develop a prototype wearable device that integrated sensor data to monitor environmental conditions.',style: TextStyle(
                            fontWeight: FontWeight.normal,
                            color: Colors.black,
                            fontSize: 14,
                            height: 1.18,
                          ),),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20,top: 0),
                          child: Text('> Successfully contributed to the construction and testing of the wearable prototype, demonstrating adaptability and problem-solving skills..',style: TextStyle(
                            fontWeight: FontWeight.normal,
                            color: Colors.black,
                            fontSize: 14,
                            height: 1.18,
                          ),),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10,top: 10),
                          child: Text('INTERNSHIP- 25 Days',style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontSize: 18,
                          ),),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10,top: 0),
                          child: Text('Mettur Thermal Power Power Plant -1',style: TextStyle(
                            fontWeight: FontWeight.normal,
                            color: Colors.black,
                            fontSize: 14,
                            // height: .8,
                          ),),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20,top: 0),
                          child: Text('> Assisted in equipment operation & maintenance, ensuring efficiency.\n> Collaborated with team on troubleshooting & safety improvements.\n> Gained valuable technical and teamwork skills during the internship.',style: TextStyle(
                            fontWeight: FontWeight.normal,
                            color: Colors.black,
                            fontSize: 14,
                            height: 1.18,
                          ),),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10,top: 10),
                          child: Text('WORK EXPERIENCE - 6 months',style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontSize: 18,
                          ),),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10,top: 0),
                          child: Text('AGC Group Of Companies\nPosition: Web Developer (Part-Time)',style: TextStyle(
                            fontWeight: FontWeight.normal,
                            color: Colors.black,
                            fontSize: 14,
                            // height: .8,
                          ),),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20,top: 0),
                          child: Text('> Acquired valuable skills in team collaboration and effective time management.\n> Successfully balanced part-time employment responsibilities while contributing to project success.',style: TextStyle(
                            fontWeight: FontWeight.normal,
                            color: Colors.black,
                            fontSize: 14,
                            height: 1.18,
                          ),),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10,left: 5),
                          child: Container(
                              height: 28,
                              width: 200,
                              // color: Color(0xFF043382),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                color: Color(0xFF043382),
                                borderRadius: BorderRadius.circular(15),
                                border: Border.all(color: Color(0xFF043382),width: 2,
                                ),
                              ),
                              child: Text('Projects',
                                style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,

                                ),)),
                        ),
                        SizedBox(height: 10,),
                        Padding(
                          padding: const EdgeInsets.only(left: 20,top: 0),
                          child: Text('1. Embedded Based Smart Irrigation System.\n2. Automatic Wheelchair Using Eye Blink Sensor\n3. Automatic Waste Disposal Segregation System \n4. Electric Heated Insoles\n5. Hotstar Clone Using HTML & CSS\n6. UI Design for Digital Visiting Card App By UIzard',style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontSize: 14,
                            height: 1.6,
                          ),),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10,left: 5),
                          child: Container(
                              height: 28,
                              width: 200,
                              // color: Color(0xFF043382),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                color: Color(0xFF043382),
                                borderRadius: BorderRadius.circular(15),
                                border: Border.all(color: Color(0xFF043382),width: 2,
                                ),
                              ),
                              child: Text('Awards',
                                style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                ),)),
                        ),
                        SizedBox(height: 10,),
                        Padding(
                          padding: const EdgeInsets.only(left: 20,top: 0),
                          child: Text('1st Price    - PROJECT GCT Coimbatore\n2nd Price  - PROJECT GCE Salem\n2nd Price  - PPT Presentation GCT Coimbatore',style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontSize: 14,
                            height: 1.6,
                          ),),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10,left: 5),
                          child: Container(
                              height: 28,
                              width: 200,
                              // color: Color(0xFF043382),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                color: Color(0xFF043382),
                                borderRadius: BorderRadius.circular(15),
                                border: Border.all(color: Color(0xFF043382),width: 2,
                                ),
                              ),
                              child: Text('Certifications',
                                style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                ),)),
                        ),
                        SizedBox(height: 10,),
                        Padding(
                          padding: const EdgeInsets.only(left: 20,top: 0),
                          child: Text('1. Workshop -Industry 4.0 and Digital Supply Chain \n    @ Anna University, Chennai.\n2. Course -Web Development Using HTML & CSS\n     @ Udemy.\n3. Course - 7Day Challenge To Develop a Hotstar Clone\n     @ Devtown.',style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontSize: 14,
                            height: 1.3,
                          ),),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
             ),
           ],
         ),
    );
  }
}
